package fr.thibaudlabat.labyrinthe;


import java.io.*;
import java.util.*;

public class MazeModel implements java.io.Serializable {
    // static constants and utilities
    static public final int DEFAULT_SIZE = 15;
    static public final int INFINITY = 99999;
    static private final Random randomGenerator = new Random();

    // the model itself
    private final Integer size;
    private final boolean[][] data;
    private Coordinates begin;
    private Coordinates end;

    // dijkstra non-serializable stuff
    private transient int[][] dijkstraDistance;
    private transient boolean dijkstraCached = false;
    private transient ArrayList<Coordinates> dijkstraPath;
    private transient int maxFiniteDistance = INFINITY;
    private transient boolean exploreEveryCell = false;

    MazeModel() {
        // default size parameter
        this(DEFAULT_SIZE);
    }

    MazeModel(Integer size) {
        this.size = size;
        this.data = new boolean[size][size];
        this.dijkstraDistance = new int[size][size];
        begin = new Coordinates(0, 0);
        end = new Coordinates(size - 1, size - 1);
    }

    /**
     * Saves a {@link MazeModel} object to a file.
     *
     * @param model The model to save.
     * @param path  The path to which it will be saved.
     */
    public static void save(MazeModel model, String path) {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(model);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }


    /**
     * Tries to load a {@link MazeModel} object from a file.
     *
     * @param path The path from which we load the object.
     * @return The loaded object.
     * @throws IOException            if an error occured during the file loading
     * @throws ClassNotFoundException if something went wrong during the serialization process
     */
    public static MazeModel load(String path) throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream(path);
        ObjectInputStream in = new ObjectInputStream(fileIn);
        MazeModel model = (MazeModel) in.readObject();
        in.close();
        fileIn.close();
        return model;
    }

    public Coordinates getBegin() {
        return begin;
    }

    public void setBegin(Coordinates begin) {
        this.begin = begin;
        dijkstraCached = false;
        checks();
    }

    public Coordinates getEnd() {
        return end;
    }

    public void setEnd(Coordinates end) {
        this.end = end;
        dijkstraCached = false;
        checks();
    }

    public boolean get(int x, int y) {
        return data[x][y];
    }

    public int getSize() {
        return size;
    }

    /**
     * @param x cell's x coordinate
     * @param y cell's y coordinate
     * @return the shortest distance from the starting point to the cell
     */
    public int getDijkstraDistance(int x, int y) {
        computeDijkstra();
        return dijkstraDistance[x][y];
    }

    /**
     * @return The shortest path from the starting point to the end point.
     */
    public Iterable<Coordinates> getDijkstraPath() {
        computeDijkstra();
        return dijkstraPath;
    }

    /**
     * Randomize the current maze
     */
    public void randomize() {
        // https://algostructure.com/specials/maze.php
        // https://stackoverflow.com/questions/6083066/depth-first-search-maze-generation-algorithm-with-blocks-instead-of-walls
        begin = new Coordinates(0, 0);
        for (int i = 0; i < size; ++i)
            for (int j = 0; j < size; ++j)
                set(i, j, true);
        set(begin.x, begin.y, false);
        ArrayList<Coordinates> unvisitedCells = new ArrayList<>();
        Stack<Coordinates> stack = new Stack<>();
        for (int i = 0; 2 * i < size; ++i)
            for (int j = 0; 2 * j < size; ++j)
                unvisitedCells.add(new Coordinates(i, j));
        Coordinates current = new Coordinates(0, 0);
        unvisitedCells.remove(current);
        while (!unvisitedCells.isEmpty()) {
            ArrayList<Coordinates> neighbors = new ArrayList<>();
            Coordinates c = new Coordinates(current.x + 1, current.y);
            if (isInRange(2 * c.x, 2 * c.y) && unvisitedCells.contains(c))
                neighbors.add(c);

            c = new Coordinates(current.x - 1, current.y);
            if (isInRange(2 * c.x, 2 * c.y) && unvisitedCells.contains(c))
                neighbors.add(c);

            c = new Coordinates(current.x, current.y + 1);
            if (isInRange(2 * c.x, 2 * c.y) && unvisitedCells.contains(c))
                neighbors.add(c);

            c = new Coordinates(current.x, current.y - 1);
            if (isInRange(2 * c.x, 2 * c.y) && unvisitedCells.contains(c))
                neighbors.add(c);

            if (!neighbors.isEmpty()) {
                Coordinates next = neighbors.get(randomGenerator.nextInt(neighbors.size()));
                stack.push(current);

                // dig the whole
                int x = 2 * current.x;
                int y = 2 * current.y;
                int dx = next.x - current.x;
                int dy = next.y - current.y;
                set(x + dx, y + dy, false);
                set(x + 2 * dx, y + 2 * dy, false);

                unvisitedCells.remove(next);

                current = next;
            } else if (!stack.empty()) {
                current = stack.pop();
            } else {
                System.out.println("empty stack");
                break;
            }

        }

        // end point placement
        int s = getSize();
        if (s % 2 == 1)
            end = new Coordinates(s - 1, s - 1);
        else // s%2==0
            end = new Coordinates(s - 2, s - 2);
    }

    public void invertCell(Coordinates c) {
        set(c.x, c.y, !get(c.x, c.y));
    }


    /**
     * @return The maximum finite (not MazeModel.INFINITY) distance from the starting point to a cell
     */
    public int getMaxFiniteDistance() {
        computeDijkstra();
        return maxFiniteDistance;
    }

    private void set(int x, int y, boolean value) {
        data[x][y] = value;
        dijkstraCached = false; // invalidates dijkstra cache
        checks();
    }

    private void checks() {
        data[begin.x][begin.y] = false;
        data[end.x][end.y] = false;
    }

    private void dijkstraResetArray() {
        dijkstraDistance = new int[size][size];
        for (int i = 0; i < size; ++i)
            for (int j = 0; j < size; ++j)
                dijkstraDistance[i][j] = INFINITY;
        dijkstraDistance[begin.x][begin.y] = 0;
    }

    private boolean isInRange(int x, int y) {
        return 0 <= x && 0 <= y && x < size && y < size;
    }

    private void computeDijkstra() {
        // http://www.gitta.info/Accessibiliti/en/html/Dijkstra_learningObject1.html
        if (dijkstraCached)
            return;
        checks();
        dijkstraCached = true;
        dijkstraResetArray();


        Coordinates[][] previous = new Coordinates[size][size];
        ArrayList<Coordinates> Q = new ArrayList<>();
        for (int i = 0; i < size; ++i)
            for (int j = 0; j < size; ++j)
                Q.add(new Coordinates(i, j));

        while (
                (!exploreEveryCell && Q.contains(end))   // explores only until end point is discovered
                        || (exploreEveryCell && !Q.isEmpty()) // explores every cell
        ) {
            Q.sort(Comparator.comparingInt(c -> dijkstraDistance[c.x][c.y]));
            Coordinates u = Q.remove(0);
            List<Coordinates> neighbors = new ArrayList<>();

            Coordinates c = new Coordinates(u.x + 1, u.y);
            if (isInRange(c.x, c.y) && Q.contains(c) && !data[c.x][c.y]) neighbors.add(c);

            c = new Coordinates(u.x - 1, u.y);
            if (isInRange(c.x, c.y) && Q.contains(c) && !data[c.x][c.y]) neighbors.add(c);

            c = new Coordinates(u.x, u.y + 1);
            if (isInRange(c.x, c.y) && Q.contains(c) && !data[c.x][c.y]) neighbors.add(c);

            c = new Coordinates(u.x, u.y - 1);
            if (isInRange(c.x, c.y) && Q.contains(c) && !data[c.x][c.y]) neighbors.add(c);

            for (Coordinates n : neighbors) {
                int alt = dijkstraDistance[u.x][u.y] + 1;
                if (alt < dijkstraDistance[n.x][n.y]) {
                    dijkstraDistance[n.x][n.y] = alt;
                    previous[n.x][n.y] = u;

                }
            }
        }

        dijkstraPath = new ArrayList<>();
        if (dijkstraDistance[end.x][end.y] == INFINITY)
            return;
        Coordinates here = new Coordinates(end.x, end.y);
        dijkstraPath.add(here);
        while (!here.equals(begin)) {
            here = previous[here.x][here.y];
            dijkstraPath.add(here);
        }

        maxFiniteDistance = 1;
        for (int i = 0; i < size; ++i)
            for (int j = 0; j < size; ++j)
                if (dijkstraDistance[i][j] > maxFiniteDistance && dijkstraDistance[i][j] != INFINITY)
                    maxFiniteDistance = dijkstraDistance[i][j];
    }

    public boolean getExploreEveryCell() {
        return this.exploreEveryCell;
    }

    public void setExploreEveryCell(boolean exploreEveryCell) {
        this.exploreEveryCell = exploreEveryCell;
        dijkstraCached = false;
    }
}
