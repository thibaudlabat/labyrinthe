package fr.thibaudlabat.labyrinthe;

import java.io.Serializable;

/**
 * Represents 2D int coordinates
 */
public class Coordinates implements Serializable {
    public final int x;
    public final int y;

    public Coordinates(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int hashCode() {
        return x * 7919 + y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Coordinates) {
            Coordinates c = (Coordinates) obj;
            return (c.x == x) && (c.y == y);
        } else
            return false;
    }
}
