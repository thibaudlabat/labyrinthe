package fr.thibaudlabat.labyrinthe;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.IOException;

/**
 * Displays a maze and allows interaction with it.
 */
class MazeVue extends JPanel implements MouseInputListener, MouseMotionListener, ComponentListener {
    static private Integer L = 20; // cell size in JPanel pixels

    private final SpritesManager sprites = new SpritesManager();
    private boolean draggingBegin = false;
    private boolean draggingEnd = false;
    private MazeModel model;

    public MazeVue(MazeModel model) throws IOException {
        this.model = model;

        addMouseListener(this);
        addMouseMotionListener(this);
        addComponentListener(this);

        setSize(new Dimension(200, 200));
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (draggingBegin || draggingEnd)
            repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        paintMaze(g);
        paintDijkstraPath(g);
        paintDistance(g);
        paintBeginEndFlags(g);
        if (draggingBegin || draggingEnd)
            paintGrid(g);

    }


    @Override
    public void mousePressed(MouseEvent e) {
        Coordinates c = getMazeMouseCoordinates(e);
        if (c != null) {
            if (c.x == model.getBegin().x && c.y == model.getBegin().y)
                draggingBegin = true;
            else if (c.x == model.getEnd().x && c.y == model.getEnd().y)
                draggingEnd = true;
            else
                model.invertCell(c);
        }

        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Coordinates c = getMazeMouseCoordinates(e);
        if (draggingBegin) {
            if (c != null) {
                model.setBegin(c);
                repaint();
            }
        }
        draggingBegin = false;
        if (draggingEnd) {
            if (c != null) {
                model.setEnd(c);
                repaint();
            }
        }
        draggingEnd = false;

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void setModel(MazeModel model) {
        this.model = model;
        repaint();
    }

    @Override
    public void componentResized(ComponentEvent e) {
        Dimension d = getSize();
        int x = Math.min(d.width, d.height);
        L = x / model.getSize();
        repaint();
    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }

    private void paintMaze(Graphics g) {
        for (int i = 0; i < model.getSize(); ++i)
            for (int j = 0; j < model.getSize(); ++j) {
                Image sprite = sprites.get(model.get(i, j) ? "wall" : "ground");
                g.drawImage(sprite, L * i, L * j, L, L, null);
            }
    }

    private Color colorScale(int d) {
        float x;
        if (d == MazeModel.INFINITY) {
            x = 1;
        } else {
            x = (float) (d) / (float) (model.getMaxFiniteDistance());
            x = Math.min(Math.max(x, 0f), 1f);
            x /= 1.5f;
        }
        x = x * (float) Math.PI / 2f;


        float r = (float) Math.sin(x);
        float g = (float) Math.cos(x);
        float b = 0f;
        float alpha = 0.3f;
        return new Color(r, g, b, alpha);
    }

    private void paintDistance(Graphics g) {
        for (int i = 0; i < model.getSize(); ++i)
            for (int j = 0; j < model.getSize(); ++j) {

                if (model.get(i, j)) // wall
                    continue;

                int d = model.getDijkstraDistance(i, j);

                // on ne print pas les 99999...
                // On n'affiche plus la distance au delà d'un seuil, ça devient illisible
                if (d < MazeModel.INFINITY && model.getSize() <= 35) {
                    g.setColor(Color.PINK);
                    g.drawString(String.valueOf(d), i * L + L / 2, j * L + L / 2);
                }

                g.setColor(colorScale(d));
                g.fillRect(L * i, L * j, L, L);
            }

    }


    private void paintGrid(Graphics g) {
        for (int i = 0; i < model.getSize(); ++i)
            for (int j = 0; j < model.getSize(); ++j) {
                g.setColor(Color.BLACK);
                g.drawRect(L * i, L * j, L, L);
            }
    }

    private void paintDijkstraPath(Graphics g) {
        g.setColor(Color.BLUE);
        for (Coordinates c : model.getDijkstraPath()) {
            g.fillOval(L * c.x + L / 4, L * c.y + L / 4, L / 2, L / 2);
        }
    }


    private Coordinates getMazeMouseCoordinates(MouseEvent e) {
        Coordinates c = new Coordinates(e.getX() / L, e.getY() / L);
        if (0 <= c.x && c.x < model.getSize() && 0 <= c.y && c.y < model.getSize())
            return c;
        return null;
    }

    private void paintBeginEndFlags(Graphics g) {
        Point mouse = getMousePosition();
        if (draggingBegin) {
            if (mouse != null)
                g.drawImage(sprites.get("begin"), mouse.x - L / 2, mouse.y - L / 2, L, L, null);
        } else {
            Coordinates begin = model.getBegin();
            g.drawImage(sprites.get("begin"), begin.x * L, begin.y * L, L, L, null);
        }

        if (draggingEnd) {
            if (mouse != null)
                g.drawImage(sprites.get("end"), mouse.x - L / 2, mouse.y - L / 2, L, L, null);
        } else {
            Coordinates end = model.getEnd();
            g.drawImage(sprites.get("end"), end.x * L, end.y * L, L, L, null);
        }

    }
}
