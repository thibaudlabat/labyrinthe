package fr.thibaudlabat.labyrinthe;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.IOException;

/**
 * GUI class for the project
 * Acts as a Controller in the used Model-Vue-Controller pattern
 */
public class GUI extends JFrame {
    private final MazeVue vue;
    private MazeModel model;
    private String lastMazePath = null; // Last path used for file open/save
    private JCheckBoxMenuItem exploreEveryCellItem;

    public GUI() throws IOException {
        super("Labyrinthe - Thibaud Labat");

        model = new MazeModel();
        model.randomize();
        vue = new MazeVue(model);

        initializeGUI();
    }

    /**
     * Builds and show the window
     */
    private void initializeGUI() {
        add(vue);
        JMenuBar drawingMenuBar = new JMenuBar();
        setJMenuBar(drawingMenuBar);

        JMenu menu = new JMenu("File");
        drawingMenuBar.add(menu);

        JMenuItem newMenuItem = new JMenuItem("New Empty");
        newMenuItem.addActionListener(e -> newMaze());
        menu.add(newMenuItem);

        JMenuItem randomizeMenuItem = new JMenuItem("New Random");
        randomizeMenuItem.addActionListener(e -> randomMaze());
        menu.add(randomizeMenuItem);

        JMenuItem openMenuItem = new JMenuItem("Open");
        openMenuItem.addActionListener(e -> open());
        menu.add(openMenuItem);

        JMenuItem saveMenuItem = new JMenuItem("Save");
        saveMenuItem.addActionListener(e -> save());
        menu.add(saveMenuItem);

        JMenuItem saveAsMenuItem = new JMenuItem("Save As");
        saveAsMenuItem.addActionListener(e -> saveAs());
        menu.add(saveAsMenuItem);

        JMenuItem quitMenuItem = new JMenuItem("Quit");
        quitMenuItem.addActionListener(e -> System.exit(0));
        menu.add(quitMenuItem);

        JMenu menu2 = new JMenu("Settings");
        drawingMenuBar.add(menu2);
        exploreEveryCellItem = new JCheckBoxMenuItem("Explore every cell");
        exploreEveryCellItem.addActionListener(e -> {
            model.setExploreEveryCell(exploreEveryCellItem.getState());
            repaint();
        });
        exploreEveryCellItem.setState(model.getExploreEveryCell());
        menu2.add(exploreEveryCellItem);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(736, 782));
        pack();
        setVisible(true);

    }


    /**
     * Select a maze file path to open or save to it.
     *
     * @param saveMode true to save, false to open a file.
     * @return selected file path
     */
    private String selectMazeFile(Boolean saveMode) {
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Maze file", "maze");
        fc.setFileFilter(filter);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int approve_option = saveMode ? fc.showSaveDialog(this) : fc.showOpenDialog(this);
        if (approve_option == JFileChooser.APPROVE_OPTION) {
            String path = fc.getSelectedFile().getAbsolutePath();
            if (saveMode && !path.contains("."))
                path += ".maze";
            return path;
        }
        return null;
    }


    /**
     * Prompts for a maze file and tries to open it.
     */
    private void open() {

        String path = selectMazeFile(false);
        if (path != null) {
            try {
                setModel(MazeModel.load(path));
            } catch (Exception e) {
                String message = "Erreur lors de l'ouverture du fichier :\n" + e.getMessage();
                JOptionPane.showMessageDialog(this, message, "Erreur", JOptionPane.ERROR_MESSAGE);
            }
            lastMazePath = path;
        }
    }

    private void setModel(MazeModel newModel) {
        model=newModel;
        model.setExploreEveryCell(exploreEveryCellItem.getState());
        vue.setModel(newModel);
    }


    /**
     * Saves the maze to the last used path.
     * Uses 'saveAs' method if there is none.
     */
    private void save() {
        if (lastMazePath == null)
            saveAs();
        else
            MazeModel.save(model, lastMazePath);
    }


    /**
     * Prompts for a maze file path and saves to it.
     */
    private void saveAs() {
        String path = selectMazeFile(true);
        if (path != null) {
            MazeModel.save(model, path);
            lastMazePath = path;
        }
    }

    /**
     * Prompts for dimensions of a new maze and creates it.
     *
     * @return false if the input was invalid, true otherwise.
     */
    private boolean newMaze() {
        try {
            String sizeString = (String) JOptionPane.showInputDialog(this,
                    "Taille du nouveau labyrinthe (entre 5 et 120) ?",
                    "Nouveau labyrinthe",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    null,
                    MazeModel.DEFAULT_SIZE);
            int size = Integer.parseInt(sizeString);
            if (size < 5 || size > 120)
                throw new NumberFormatException();
           setModel(new MazeModel(size));
            lastMazePath = null;
            vue.componentResized(null);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Taille invalide.", "Erreur", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * Prompts for dimensions of a new random maze and creates it.
     */
    private void randomMaze() {
        if (newMaze()) {
            model.randomize();
        }
    }

}
