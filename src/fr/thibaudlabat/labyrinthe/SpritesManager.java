package fr.thibaudlabat.labyrinthe;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * Sprites loader
 */
public class SpritesManager {
    private final HashMap<String, Image> sprites = new HashMap<>();

    public SpritesManager() throws IOException {
        loadFile("wall", "wall.jpg");
        loadFile("ground", "ground.jpg");
        loadFile("begin", "begin.png");
        loadFile("end", "end.png");
    }

    private void loadFile(String spriteName, String fileName) throws IOException {
        //File f = new File("resources/" + fileNam);
        InputStream f = getClass().getResourceAsStream("/"+fileName);
        sprites.put(spriteName, ImageIO.read(f));
    }

    public Image get(String name) {
        return sprites.get(name);
    }
}
